//
// This program demonstrates the use of recursion to evaluate the
// Collatz Sequence
//

#include <iostream>
#include <iomanip>
#include <cstdlib>

//
// Define my own counting type
//
typedef unsigned long count_t;

//
// compute "op(n)", at step "count", stop at value "term"
//
static void
sequence(count_t (*op)(count_t),
         count_t n,
         count_t count,
         count_t term)
{
    using namespace std;

    cout << setw(12) << left << count << n << "\n";

    if (n != term) {
        sequence(op, op(n), count + 1, term);
    }
}

//
// if odd, *3 and +1; if even, divide by 2
//
static count_t
collatz(count_t n)
{
    if (n & 1) {
        return (n * 3) + 1;
    }
    else {
        return n / 2;
    }
}

int
main(int argc, char *argv[])
{
    count_t startval = 10;
    const count_t stopval = 1;
    const count_t nth = 1;

    // if command-line argument, treat it as start number
    if (argc > 1) {
        startval = strtoul(argv[1], NULL, 10);
    }

    sequence(collatz, startval, nth, stopval);

    return 0;
}
