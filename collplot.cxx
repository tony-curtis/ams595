//
// Compute the Collat sequence for numbers 1..N, the output can then
// be plotted by a tool such as "gnuplot".
//

#include <iostream>
#include <cstdlib>

static unsigned long
collatz(unsigned long v)
{
    return (v & 1) ? (3 * v) + 1 : v / 2;
}

static unsigned long
collatz_sequence(unsigned long n, unsigned long v)
{
    return (v == 1) ? n : collatz_sequence( n + 1, collatz(v) );
}

int
main(int argc, char *argv[])
{
    unsigned long n = 10;

    using namespace std;

    if (argc > 1) {
        n = strtoul(argv[1], NULL, 10);
    }

    for (unsigned long i = 1; i <= n; i += 1) {
        cout << i << " " << collatz_sequence(1, i) << "\n";
    }

    return 0;
}
