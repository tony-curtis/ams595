#include <iostream>

using namespace std;

int
main()
{
  int n = 4;

  cout << n << "\n";            // print n = 4

  cout << n++ << "\n";          // cout gets n = 4, then incremented to 5

  cout << ++n << "\n";          // increment n first, cout gets 6

  cout << n << "\n";            // print 6

  n++;                          // increment n, ignore old value

  cout << n << "\n";            // print 7

  return 0;
}
