//
// Demonstrate how to use the various different styles of iterator
// available in C++
//

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

using namespace std;

typedef float myelt;
typedef vector<myelt> myvec;

void
inspect(const myelt& n)
{
    cout << ' ' << n;
}

void
call_it(string prefix,
        const myvec& v,
        void thing(const myvec& v))
{
    cout << setw(16)
         << left
         << prefix + ":";

    thing(v);

    cout << "\n" << "\n";
}

// this is sort of like good old-fashioned C

void
c_style_indexed(const myvec& v)
{
    for (myvec::size_type i = 0; i < v.size(); i += 1) {
        inspect(v[i]);
    }
}

// range over the extent of the vector

void
iterator_loop(const myvec& v)
{
    for (auto it = v.begin(); it < v.end(); it += 1) {
        inspect(*it);
    }
}

// encapsulate range

void
map_style(const myvec& v)
{
    for_each(v.begin(), v.end(), inspect);
}

// extract items implicitly from vector (like a list comprehension)

void
range_style(const myvec& v)
{
    for (const auto& n : v) {
        inspect(n);
    }
}

int
main()
{
    myvec vic {5.207, 14.451, -0.4546, 2.4478, 1.131};

    call_it("C style indexed", vic, c_style_indexed);
    call_it("Iterator loop", vic, iterator_loop);
    call_it("Map style", vic, map_style);
    call_it("Range style", vic, range_style);

    return 0;
}
