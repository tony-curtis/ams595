#
# choose compiler and flags to pass to it
#
CXX         = g++
DEBUG       = -ggdb
OPT         = -O3
STD_CONFORM = -std=c++11 -pthread
WARNINGS    = -Wall -Wextra -pedantic

#
# internal setup
#
CXXFLAGS    = $(DEBUG) $(OPT) $(STD_CONFORM) $(WARNINGS)
LIBS        =

#
# files to compile
#
SOURCES     = $(wildcard *.cxx)
OBJECTS     = $(SOURCES:.cxx=.o)
EXES        = $(SOURCES:.cxx=.x)

#
# targets that aren't files-to-be-created
#
.PHONY: all clean

#
# tell make about C++ source and resultant executables
#
.SUFFIXES: .cxx .x

#
# how to compile & link C++ 
#
.cxx.x:
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LIBS)

#
# main targets (default: all)
#
all:	$(EXES)

clean:
	rm -f $(EXES)
	rm -rf *.dSYM        # Mac
