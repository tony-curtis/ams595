// requires: std=c++11, pthreads

#include <unistd.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <thread>

// pass data into threads

struct thread_arg {
    unsigned id;
};

// called by each thread

static void
something(const thread_arg& a)
{
    // pack into stream to go out in 1 burst
    std::ostringstream o;

    o << "Hello from thread " << a.id << "\n";

    std::cout << o.str();
}

int
main(int argc, char *argv[])
{
    // how many threads
    const unsigned nthreads = (argc > 1) ?
        std::stoi(argv[1]) :
        sysconf(_SC_NPROCESSORS_ONLN);

    // allocate thread args and tracker
    thread_arg* ta = new thread_arg[nthreads];
    std::vector <std::thread> ts;

    // start threads, keep track of them
    for (unsigned i = 0; i < nthreads; ++i) {
        ta[i].id = i;
        ts.push_back(std::thread(something, ta[i]));
    }

    // wait for threads to finish
    for (auto& t : ts) {
        t.join();
    }

    delete [] ta;

    return 0;
}
