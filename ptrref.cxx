//
// Demonstrate the use of different "call by" techniques, specifically
// to show the difference between pointers (inherited from C) and
// references (C++ only, much easier to use).
//

#include <iostream>
#include <iomanip>
#include <string>

void
show(std::string prefix, int v)
{
    using namespace std;

    const string s = prefix + ":";

    cout << setw(20) << left << s << v << "\n";
}

//
// pointer: programmer must dereference "v" for each use
//
void
add2_by_ptr(int* v)
{
    *v += 2;
}

//
// reference: programmer just uses "v" as usual, compiler handles
// dereferences
//
void
add2_by_ref(int& v)
{
    v += 2;
}

//
// value: (just because) local arg gets changed, but not the caller
//
void
add2_by_val(int v)
{
    v += 2;
}

int
main()
{
    int x = 4;

    show("initial", x);

    add2_by_ptr(&x);            // have to pass pointer to "x"

    show("by pointer", x);

    add2_by_ref(x);             // just pass "x"

    show("by reference", x);

    add2_by_val(x);             // just pass "x"

    show("by value", x);

    return 0;
}
